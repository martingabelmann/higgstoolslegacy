#!/usr/bin/env python3
from argparse import ArgumentParser
from os import path
import Higgs.bounds as HB
import Higgs.tools.Input as hinput
import Higgs.signals as HS

# change paths here or via command line arguments -b / -s
DEFAULTHB = "~/FeynTools/hbdataset"
DEFAULTHS = "~/FeynTools/hsdataset"


def writeSLHA(slhafile, hbresult, chisq):
    obsMax = [0,0]
    for k,v in hbresult.selectedLimits.items():
        obsRatio = v.obsRatio()
        if obsRatio > obsMax[1]:
            obsMax = [k, obsRatio]

    with open(slhafile, 'a') as f:
        f.write('#\n')
        f.write('Block HiggsBoundsResults\n')
        f.write(f' 0    0    {obsMax[0]}  # particle with max obsRatio\n')
        f.write(f' 0    1    {obsMax[1]}  # max obsRatio\n')
        f.write(f' 0    2    {int(hbresult.allowed)}  # scenario allowed (1: allowed, 0: excluded)\n')
        cid = 0
        for k,v in hbresult.selectedLimits.items():
            limit = v.limit()
            desc = f'{limit.processDesc()} ({limit.reference()})'
            f.write(f' {cid}    0    {k}  # particle id\n {cid}    1    {v.obsRatio()}  # obsRatio\n {cid}    2    {v.expRatio()}  # expected\n {cid}    3    ||{desc}||  # description\n')
            cid += 1

        f.write('#\n')
        f.write('Block HiggsSignalsResults\n')
        f.write(f' 8    {signals.observableCount()}  # total observables\n')
        f.write(f' 17   {chisq}  # total chi^2\n')

def runHSHB(slhafile):
    # use SLHA file as input for HiggsPredictions
    dc = hinput.readHB5SLHA(slhafile, neutralIds, chargedIds)
    pred = hinput.predictionsFromDict(dc, neutralIdStrings, chargedIdStrings, [])
    for p in pred.particleIds():
        pred.particle(p).setMassUnc(args.mass_uncert)

    # evaluate HiggsBounds
    hbresult = bounds(pred)
    if args.verbose > 0:
        print(hbresult)

    # evaluate HiggsSignals
    chisq = signals(pred)
    if args.verbose > 0:
        print(f"HiggsSignals chisq: {chisq}")

    return chisq, hbresult, pred

parser = ArgumentParser(description='Legacy tool/wrapper to make old scan scripts compatible with new HiggsTools (HiggsBounds/HiggsSignals 2210.09332)')
parser.add_argument("slhafile", nargs = '+', help = 'path to SLHA file. Passing multiple files (space separated) is also supported')
parser.add_argument("-c", '--charged',   nargs='+', default=[], help = 'PDG IDs of charged particles (space separated)')
parser.add_argument("-n", '--neutral',   nargs='+', default=[25], help = 'PDG IDs of neutral particles (space separated)')
parser.add_argument("-b", '--hbdataset', type=str, default=DEFAULTHB, help = 'path to HB dataset')
parser.add_argument("-s", '--hsdataset', type=str, default=DEFAULTHS, help = 'path to HS dataset')
parser.add_argument("-W", '--no-write',  action='store_true', help = 'don\'t write restult to the SLHA files')
parser.add_argument("-m", '--mass-uncert',  type=float, help = 'generic mass uncertainty for all particles', default = 3)
parser.add_argument("-v", '--verbose',  action='count', help = 'print results to stdout', default = 0)
parser.add_argument("-i", '--interactive',  action='store_true', help = 'open an interactive session after results have been obtained')

args = parser.parse_args()

# initialize HiggsBounds and Higgs signals
bounds = HB.Bounds(path.expanduser(args.hbdataset))
signals = HS.Signals(path.expanduser(args.hsdataset))

# SLHA particle IDs for neutral and charged scalars
neutralIds = [int(id) for id in args.neutral]
chargedIds = [int(id) for id in args.charged]
neutralIdStrings = [str(id) for id in neutralIds]
chargedIdStrings = [str(id) for id in chargedIds]

results = []
for f in args.slhafile:
    chisq, hbresult,  pred = runHSHB(f)
    results.append([chisq, hbresult, pred])
    if args.no_write:
        continue
    writeSLHA(f, hbresult, chisq)

if args.interactive:
    from IPython import embed
    print('results are stored in the list "results" which is of the form results=[[hbresult, chisq], [hbresult, chisq], ...]  where each entry corresponds to the result of the n-th supplied SLHA file')
    embed()
    exit(0)
