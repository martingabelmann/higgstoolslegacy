# HiggsTools (HB/HS) Legacy
Legacy tool/wrapper to make old scan scripts compatible with new HiggsTools i.e. HiggsBounds/HiggsSignals, see [arXiv:2210.09332](https://arxiv.org/abs/2210.09332) and the [https://gitlab.com/higgsbounds/higgstools](HiggsTools) repository.

## Usage
The path to the HiggsBounds/HiggsSignals datasets need to be provided, either by the `-b`/`-s` options or by hardcoding them at the top of the HSHBlegacy.py file. Afterwards running:

```bash
./HBHSlegacy.py path/to/slha.file -c 37 -n 25 35 36 45
```
will write the HB/HS result to the `slha.file` using (partially) the LHA-conventions of the old HB/HS routines. The options `-c` and `-n` specify the charged/neutral PDG ids.  

One can also supply multiple SLHA files:
```bash
/HBHSlegacy.py file1 file2 ... -c 37 -n 25 35 36 45
```
The option `-v` additionally prints the results to the terminal. The option `-W` prevents writing tho the slha file(s). The option `-i` opens an interactive session after results have been obtained such that they can be investigated in more detail.

For more details issue the help command:
```bash
./HBHSlegacy.py -h
```
